package main

import (
	"strings"

	"codeberg.org/serghei/wurm/internal/config"
	"codeberg.org/serghei/wurm/pkg/wurm"
	"github.com/spf13/cobra"
)

const VERSION = "2.00-alpha"

var initialize = &cobra.Command{
	Use:   "init",
	Short: "Create a wurm.toml at the root of the directory",
	Long:  "Create a wurm.toml containing default configurations at the root of the current directory",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		config.CreateFile()
	},
}

var watch = &cobra.Command{
	Use:   "watch",
	Short: "Watch the specified paths for changes",
	Long:  "Watch the specified paths for changes, optionally doing something when a change occurs",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		watcher := wurm.New()
		watcher.AddCommands(config.Command)
		watcher.Ignore(strings.Split(config.Ignore, " ")...)
		watcher.Watch(args...)
	},
}

var run = &cobra.Command{
	Use:   "run",
	Short: "Runs the specified command as defined in the wurm.toml file",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		wurm.Run(args[0])
	},
}

func main() {
	initWatchFlags(watch)

	root := &cobra.Command{
		Use:     "wurm [cmd] [wurm]",
		Version: VERSION,
		Short:   "File watcher",
	}

	root.AddCommand(
		initialize,
		watch,
		run,
	)
	root.Execute()
}

func initWatchFlags(cmd *cobra.Command) {
	cmd.Flags().StringVarP(&config.Command, "command", "c", "", "Command/s to be executed on change")
	cmd.Flags().StringVarP(&config.Ignore, "ignore", "i", "", "Files and/or directories to be excluded from the watch")
	cmd.Flags().StringVarP(&config.Pattern, "pattern", "p", "", "Regular expression pattern that watched files must match")

	cmd.Flags().BoolVarP(&config.Recursive, "recursive", "r", false, "Traverse and watch entries of child directories")
}
