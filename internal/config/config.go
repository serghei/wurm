package config

import (
	"os"
	"regexp"

	"codeberg.org/serghei/wurm/internal/notify"
	"github.com/BurntSushi/toml"
)

const (
	ConfigFile    = "Wurm.toml"
	configDefault = `[global]
ignore_global = false
recursive = false
ignore = [".git"]
commands = []
paths = []
`
)

var (
	Command         string
	Ignore          string
	Pattern         string
	PatternCompiled *regexp.Regexp
	Recursive       bool
)

type TableAppender interface {
	appendTo(*Table)
}

type Config struct {
	Global GlobalTable
	Common map[string]Table
	Runner map[string]Table
	Meta   toml.MetaData
}

type GlobalTable struct {
	IgnoreGlobal bool `toml:"ignore_global"`
	Recursive    bool
	Pattern      string

	Commands []string
	Ignore   []string
	Paths    []string
}

type Table struct {
	Use []string
	GlobalTable
}

func CreateFile() {
	if stat, err := os.Stat(ConfigFile); err == nil && !stat.IsDir() {
		notify.ErrConfigExists()
		os.Exit(1)
	}

	file, err := os.Create(ConfigFile)
	if err != nil {
		notify.ErrSystem(err)
		os.Exit(1)
	}
	defer file.Close()

	file.WriteString(configDefault)
}

func ReadFile() (config *Config) {
	if meta, err := toml.DecodeFile(ConfigFile, &config); err != nil {
		config = newConfig()
	} else {
		config.Meta = meta
	}

	return config
}

func newConfig() *Config {
	return &Config{
		Global: GlobalTable{
			Commands: []string{},
			Ignore:   []string{},
			Paths:    []string{},
		},
	}
}

func (t *Table) Append(table TableAppender) {
	table.appendTo(t)
}

func (t Table) appendTo(table *Table) {
	table.Ignore = append(table.Ignore, t.Ignore...)
	table.Paths = append(table.Paths, t.Paths...)
	table.Commands = append(table.Commands, t.Commands...)
}

func (t GlobalTable) appendTo(table *Table) {
	table.Ignore = append(table.Ignore, t.Ignore...)
	table.Paths = append(table.Paths, t.Paths...)
	table.Commands = append(table.Commands, t.Commands...)
}
