# Wurm

Wurm is a command-line tool that executes a command when the contents of given files or directories change.

## Content

- [Installation](#installation)
    - [With Go](#with-go)
- [Commands](#commands)
    - [watch](#watch)
        - [Flags](#flags)
            - [command](#command)
            - [ignore](#ignore)
            - [recursive](#recursive)
            - [pattern](#pattern)
    - [run](#run)
    - [init](#init)
    - [Configuration File](#configuration-file)
        - [global](#global)
        - [common](#common)
        - [runner](#runner)

# Installation

## With `go`

```sh
go install codeberg.org/serghei/wurm@latest
```

# Commands

## watch

`watch` requires at least one argument that is the path to a file or directory.

The following command watches all files and directories for changes in the current directory:

```sh
wurm watch .
```

Without flags, this will only print when the content of the directory changes and prints out the affected path.

---

You can also watch to multiple files and directories:

```sh
wurm watch index.html index.js
```

### Flags

You can use flags to execute one or more commands when the content of the files or directories changes. All the flags are usable together.

#### command

`command` specifies a command to execute on every change. If the command includes whitespace, use quotation marks.

```sh
wurm watch src --command make
wurm watch src --command "go build ./cmd/app"
# or
wurm watch src -c make
wurm watch src -c "go build ./cmd/app"
```

#### ignore

`ignore` specifies paths that should not be watched for changes. When passing multiple files or directories, use quotation marks.

```sh
wurm watch . --ignore dist
wurm watch . --ignore "dist doc"
# or
wurm watch . -i dist
wurm watch . -i "dist doc"
```

#### recursive

`recursive` specifies that Wurm should also watch directories contained in the passed paths directories.

```sh
wurm watch . --recursive
# or
wurm watch . -r
```

#### pattern

`pattern` specifies a regular expression pattern that the name of a file must match to be watched.

```sh
wurm watch . --pattern ".*\.go"
# or
wurm watch . -p ".*\.go"
```

## run

`run` requires exactly one argument that exists as a table in the `Wurm.toml` configuration file.

```sh
wurm run check
```

## init

`init` takes no arguments and generates a `Wurm.toml` configuration file in the root of the current directory with some default values.

```sh
wurm init
```

# Configuration File

The `Wurm.toml` configuration file contains three types of configuration: `global`, `common`, and `runner`. Unless ignored, Wurm will hot reload modifications to the `Wurm.toml`.

## global

When using the `init` command, a `Wurm.toml` is generated with the following structure:

```toml
[global]
ignore_global = false
recursive = false
pattern = ".*\.toml"
ignore = [".git"]
commands = []
paths = []
```

It contains a table with global configurations that are usable by the `common` and `runner` tables. The global configuration is used by the `watch` and `runner` commands.

- ignore_global (boolean): If true, ignores the the global configuration.
- recursive (boolean): See `recursive` flag.
- pattern (string): See `pattern` flag.
- ignore (string array): See `ignore` flag.
- commands (string array): See `commands` flag.
- paths (string array): The paths to be watched.

All these values are optional and can also be specified in `common` and `runner` tables. Primitive types, like booleans, are overwritten by `common` and `runner` tables, while arrays are extended.

## common

A `common` table contains values that are usable by other `common` or `runner` tables through the `use` key.

```toml
[common.paths]
paths = ["src"]

[common.dev]
use = ["paths"]
paths = ["lib", "shared"]
```

In this example the `common.dev` table extends the paths specified in the `common.paths` table by `lib`. A runner table using `common.dev` would now include both paths. The `use` key *does not* modify the included tables.

## runner

A `runner` table contains predefined arguments for usage with the `run` command.

```toml
[common.paths]
paths = ["src"]

[common.dev]
use = ["paths"]
paths = ["lib", "shared"]

[runner.build]
use = ["dev"]
recursive = true
commmands = ["go build ./cmd/app"]
ignore = ["target"]
paths = ["."]
```

When running `wurm run build` it reads the runner table with the same name and uses the specified values.

This example can be translated to the following shell command:

```sh
wurm watch . src lib shared -r -c "go build ./cmd/app" -i target
```

Note that duplicated paths are not watched multiple times. Given the previous example, if `lib` is a child of the `src` directory, `lib`, as an argument, is skipped. It is still watched.

All tables must use the prefix associated with their type. When specifying tables for usage through the `use` key, the prefix is not needed.
